/**
 * Created By: Clark Tomlinson  <${personalEmail}>
 * On: 11/4/14, 1:29 PM
 * Url: ${personalWebsite}
 * Copyright Clark Tomlinson © 2014
 *
 */
$(function () {
    var ul = $('#upload ul');

    $('#drop a').on('click', function () {
        // Simulate a click on the file input button
        $(this).parent().find('input').click();
    });

    // Initalize the jQuery File Upload Plugin
    $('#upload').fileupload({
        // This element will accept file drag/drop uploading
        dropZone: $('#drop'),

        // Limit the max droped files to 1
        limitMultiFileUploads: 1,

        // Upload each file as its own request
        singleFileUploads: true,

        /*
         * This function is called when a file is added to the queue;
         * either via browse button or drop
         */
        add: function (e, data) {
            var tpl = $('<li class="working"><input type="text" value="0" data-width="48" data-height="48"' +
            ' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" /><p></p><span></span></li>');

            tpl.find('p').text(data.files[0].name).append('<i>' + formatFileSize(data.files[0].size) + '</li>');

            // Add the HTML to the UL element
            data.context = tpl.appendTo(ul);

            // Initalize the knob
            tpl.find('input').knob();

            // Listen for clicks on the cancel button
            tpl.find('span').on('click', function () {
                if (tpl.hasClass('working')) {
                    jqHXR.abort();

                    tpl.fadeOut(function () {
                        tpl.remove();
                    });
                }
            });

            var jqHXR = data.submit();
        },
        progress: function (e, data) {
            // Calculate the completion percentage of the upload
            var progress = parseInt(data.loaded / data.total * 100, 10);

            /*
             * Update the hidden input field and trigger a change
             * so that jQuery knob plugin knows to update the dial
             */
            data.context.find('input').val(progress).change();

            if (progress === 100) {
                data.context.removeClass('working');
            }
        },
        done: function (e, data) {
            console.log(data);
            var result = JSON.parse(data.result);
            console.log(result);
            var tpl = $('<a data-html="true" data-toggle="popover" data-content="" class="view-report"><i>Report</i></a>');

            data.context = tpl.appendTo(data.context.find('span'));

            var content = 'File Name: ' + result.fileName + '<br />Selectors: ' + result.selectors + '<br /> Colors: ' + result.colors ;

            tpl.popover({
                trigger: 'click',
                content: content,
                html: true
            });


        },
        fail: function (e, data) {
            // Something has gone horribly horribly wrong
            data.context.addClass('error');
        }
    });

    // Prevent the default action when a file is dropped on the window
    $(document).on('drop dragover', function (e) {
        e.preventDefault();
    });

    // Helper function that formats the filesize
    function formatFileSize(bytes) {
        if (typeof bytes !== 'number') {
            return '';
        }

        if (bytes >= 1000000000) {
            return (bytes / 1000000000).toFixed(2) + ' GB';
        }

        if (bytes >= 1000000) {
            return (bytes / 1000000).toFixed(2) + ' MB';
        }

        return (bytes / 1000).toFixed(2) + ' KB';
    }

});
