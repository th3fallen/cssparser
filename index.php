<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title></title>

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="assets/vendor/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="assets/css/style.css"/>
</head>
<body>
<?php
/**
 * @author Clark Tomlinson  <fallen013@gmail.com>
 * @since 10/22/14, 11:33 AM
 * @link http:/www.clarkt.com
 * @copyright Clark Tomlinson © 2014
 *
 */

require_once 'vendor/autoload.php';
require_once 'vendor/illuminate/support/Illuminate/Support/helpers.php';

?>

<form id="upload" action="upload.php" enctype="multipart/form-data" method="post">
    <div id="drop">
        Drop Here
        <a>Browse</a>
        <input type="file" name="cssFile"/>
    </div>
    <ul></ul>

    <!--    <input type="submit" value="Upload File"/>-->
</form>

<script type="text/javascript" src="assets/vendor/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="assets/vendor/bootstrap/dist/js/bootstrap.js"></script>
<script type="text/javascript" src="assets/vendor/blueimp-file-upload/js/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="assets/vendor/blueimp-file-upload/js/vendor/jquery.ui.widget.js"></script>
<script type="text/javascript" src="assets/vendor/blueimp-file-upload/js/jquery.fileupload.js"></script>
<script type="text/javascript" src="assets/vendor/aterrien/jQuery-Knob/js/jquery.knob.js"></script>
<script type="text/javascript" src="assets/js/app.js"></script>
</body>
</html>