<?php
/**
 * @author Clark Tomlinson  <fallen013@gmail.com>
 * @since 10/22/14, 11:59 AM
 * @link http:/www.clarkt.com
 * @copyright Clark Tomlinson © 2014
 *
 */

namespace cssParser;


class ReportGenerator
{
    /**
     * @var int
     */
    public $colors = 0;
    /**
     * @var int
     */
    public $selectors = 0;
    /**
     * @var resource
     */
    protected $file;
    /**
     * @var string
     */
    protected $fileName;
    /**
     * @var string
     */
    protected $filePath;

    /**
     * @param $fileName
     * @param $filePath
     */
    public function __construct($fileName, $filePath)
    {
        $this->fileName = $fileName;
        $this->filePath = $filePath;
        $this->file = fopen($filePath . DIRECTORY_SEPARATOR . $fileName, 'r');
        $this->readFile();
    }

    /**
     *
     */
    private function readFile()
    {
        while (!feof($this->file)) {
            $line = fgets($this->file);
            $this->countSelectors($line);
            $this->countColors($line);
        }
        fclose($this->file);
    }

    /**
     * @param $line
     */
    public function countSelectors($line)
    {
        $match = preg_match('/\{.+?\}|,/s', $line);

        if ($match === 1) {
            $this->selectors++;
        }
    }

    /**
     * @param $line
     */
    public function countColors($line)
    {
        $match = preg_match('/#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?\b/', $line);

        if ($match === 1) {
            $this->colors++;
        }
    }

    /**
     * @return array
     */
    public function generateReports()
    {
        $report = array(
            'fileName' => $this->fileName,
            'selectors' => $this->selectors,
            'colors' => $this->colors,
        );

        $data = json_encode($report);
        $ds = DIRECTORY_SEPARATOR;
        $filePath = dirname($this->filePath) . $ds . 'reports' . $ds . $this->fileName . '.report.json';

        file_put_contents($filePath, $data);

        return $data;
    }

}