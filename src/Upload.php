<?php
/**
 * @author Clark Tomlinson  <fallen013@gmail.com>
 * @since 10/22/14, 11:34 AM
 * @link http:/www.clarkt.com
 * @copyright Clark Tomlinson © 2014
 *
 */

namespace cssParser;

use Upload\File;
use Upload\Storage\FileSystem;
use Upload\Validation\Mimetype;
use Upload\Validation\Size;

class Upload
{
    /**
     * @var FileSystem
     */
    private $storage;
    /**
     * @var File
     */
    private $file;

    /**
     * Stores all accepted types in case we want to add more
     *
     * @var array
     */
    private $acceptedTypes = array(
        'text/css',
        'text/plain'
    );

    /**
     * @param FileSystem $storage
     * @param string $fieldName
     */
    public function __construct(FileSystem $storage, $fieldName = 'cssFile')
    {
        $this->storage = $storage;
        $this->file = new File($fieldName, $this->storage);

    }

    /**
     * @return array|string
     */
    public function upload()
    {
        // Validate our file
        $this->validateFile();

        // Attempt to upload
        try {
            $this->file->upload();
            return $this->file->getNameWithExtension();
        } catch (\Exception $e) {
            return $this->file->getErrors();
        }
    }

    /**
     * Validates file mimetypes to make sure they are allowed
     */
    private function validateFile()
    {
        $this->file->addValidations(array(
            // Verify the uploaded file is an accepted type
            new Mimetype($this->acceptedTypes),
            // Verify the filesize is no larger than 5M (seriously who has a 5M css file?)
            new Size('5M')
        ));
    }
}