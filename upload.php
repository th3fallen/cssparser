<?php
/**
 * @author Clark Tomlinson  <fallen013@gmail.com>
 * @since 10/22/14, 11:45 AM
 * @link http:/www.clarkt.com
 * @copyright Clark Tomlinson © 2014
 *
 */

use cssParser\ReportGenerator;

require_once 'vendor/autoload.php';
$filePath = realpath('storage/files');

$file = new cssParser\Upload(new Upload\Storage\FileSystem($filePath, true));
$output = $file->upload();

$parser = new ReportGenerator($output, $filePath);

echo $parser->generateReports();